package main

import (
	"bytes"
	"math/rand"
	"time"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

var (
	japaneseKatakana = []int{12449, 12531}
	japaneseHiragana = []int{12353, 12435}
)

type cracker struct {
	*cview.TextView

	lastUpdate time.Time
	b          bytes.Buffer
	text       []rune
}

func newCracker() *cview.Window {
	c := &cracker{TextView: cview.NewTextView()}
	c.SetScrollBarVisibility(cview.ScrollBarNever)

	w := cview.NewWindow(c)
	w.SetTitle("Cracker")

	return w
}

func (c *cracker) SetRect(x, y, width, height int) {
	_, _, rwidth, rheight := c.GetRect()
	if width == rwidth && height == rheight {
		c.Box.SetRect(x, y, width, height)
		return
	}

	c.Box.SetRect(x, y, width, height)
	c.fillText()
}

func (c *cracker) fillText() {
	_, _, width, height := c.GetInnerRect()

	if len(c.text) != height*(width/2) {
		c.b.Reset()
		for i := 0; i < height; i++ {
			for j := 0; j < width/2; j++ {
				start, end := japaneseHiragana[0], japaneseHiragana[1]
				if rand.Intn(2) == 0 || true {
					start, end = japaneseKatakana[0], japaneseKatakana[1]
				}
				c.b.WriteRune(rune(start + rand.Intn(end-start)))
			}
		}
		c.text = []rune(c.b.String())
		c.SetText(string(c.text))
		return
	}

	for i := 0; i < len(c.text); i++ {
		if rand.Intn(100) == 0 {
			start, end := japaneseHiragana[0], japaneseHiragana[1]
			if rand.Intn(2) == 0 || true {
				start, end = japaneseKatakana[0], japaneseKatakana[1]
			}
			c.text[i] = rune(start + rand.Intn(end-start))
		}
	}
	c.SetText(string(c.text))
}

func (c *cracker) Draw(screen tcell.Screen) {
	if time.Since(c.lastUpdate) > 100*time.Millisecond {
		c.fillText()
		c.lastUpdate = time.Now()
	}

	c.TextView.Draw(screen)
}
