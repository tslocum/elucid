# elucid
[![CI status](https://gitlab.com/tslocum/elucid/badges/master/pipeline.svg)](https://gitlab.com/tslocum/elucid/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Multiplayer hacking simulation game

## Download

```bash
go get gitlab.com/tslocum/elucid
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/elucid/issues).
