package main

import (
	"log"
	"time"

	"gitlab.com/tslocum/cview"
)

const refreshRate = time.Second / 60

var app *cview.Application

func handleDraw() {
	t := time.NewTicker(refreshRate)
	for range t.C {
		app.Draw()
	}
}

func main() {
	app = cview.NewApplication()
	app.EnableMouse(true)

	wm := cview.NewWindowManager()

	c := newCracker()

	wm.Add(c)

	app.SetRoot(wm, true)

	go handleDraw()

	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
