module gitlab.com/tslocum/elucid

go 1.15

require (
	github.com/gdamore/tcell/v2 v2.0.1-0.20201109052606-7d87d8188c8d
	gitlab.com/tslocum/cview v1.5.2-0.20201114045554-68b2c49d115a
)
